import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:ipalsistemaerasiapp/halaman/halaman_bak_sedimentasi_bau.dart';
import 'package:ipalsistemaerasiapp/halaman/halaman_bak_sedimentasi_lumpur_naik.dart';
import 'package:ipalsistemaerasiapp/halaman/halaman_bak_sedimentasi_overflow_cokelat.dart';
import 'package:ipalsistemaerasiapp/halaman/halaman_bak_sedimentasi_overflow_kuning.dart';

class BakSedimentasi extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          body: ListView(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(top: 35, left: 33),
            child: Align(
              alignment: Alignment.topRight,
              child: SizedBox(
                height: 33,
                child: TextButton(
                  child: const Text(
                    'Kembali Ke Halaman Sebelumnya',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Color(0xFF000000),
                        fontFamily: 'Roboto',
                        fontSize: 11,
                        letterSpacing: 0,
                        fontWeight: FontWeight.normal,
                        height: 1),
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 35),
            child: Align(
              alignment: Alignment.topCenter,
              child: SizedBox(
                child: Text(
                  "Bak Sedimentasi",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Color(0xFF000000),
                      fontFamily: 'Roboto',
                      fontSize: 24,
                      letterSpacing: 0,
                      fontWeight: FontWeight.normal,
                      height: 1),
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(right: 33, top: 40, left: 33),
            child: Align(
              alignment: Alignment.topCenter,
              child: SizedBox(
                child: MaterialButton(
                  minWidth: 239,
                  height: 62,
                  color: Color(0xFFFFFFFF),
                  onPressed: () {
                    Get.to(BakSedimentasiLumpurNaik());
                  },
                  child: Text(
                    "Lumpur Naik",
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                        color: Color.fromRGBO(0, 0, 0, 1),
                        fontFamily: 'Roboto',
                        fontSize: 18,
                        letterSpacing: 0,
                        fontWeight: FontWeight.normal,
                        height: 1),
                  ),
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(right: 33, top: 40, left: 33),
            child: Align(
              alignment: Alignment.topCenter,
              child: SizedBox(
                child: MaterialButton(
                  minWidth: 239,
                  height: 62,
                  color: Color(0xFFFFFFFF),
                  onPressed: () {
                    Get.to(BakSedimentasiOverflowKuning());
                  },
                  child: Text(
                    "Air Overflow Kuning",
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                        color: Color.fromRGBO(0, 0, 0, 1),
                        fontFamily: 'Roboto',
                        fontSize: 18,
                        letterSpacing: 0,
                        fontWeight: FontWeight.normal,
                        height: 1),
                  ),
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(right: 33, top: 40, left: 33),
            child: Align(
              alignment: Alignment.topCenter,
              child: SizedBox(
                child: MaterialButton(
                  minWidth: 239,
                  height: 62,
                  color: Color(0xFFFFFFFF),
                  onPressed: () {
                    Get.to(BakSedimentasiOverflowCokelat());
                  },
                  child: Text(
                    "Air Overflow Cokelat",
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                        color: Color.fromRGBO(0, 0, 0, 1),
                        fontFamily: 'Roboto',
                        fontSize: 18,
                        letterSpacing: 0,
                        fontWeight: FontWeight.normal,
                        height: 1),
                  ),
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(right: 33, top: 40, left: 33),
            child: Align(
              alignment: Alignment.topCenter,
              child: SizedBox(
                child: MaterialButton(
                  minWidth: 239,
                  height: 62,
                  color: Color(0xFFFFFFFF),
                  onPressed: () {
                    Get.to(BakSedimentasiBau());
                  },
                  child: Text(
                    "Air Overflow dan/atau Lumpur Bau ",
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                        color: Color.fromRGBO(0, 0, 0, 1),
                        fontFamily: 'Roboto',
                        fontSize: 18,
                        letterSpacing: 0,
                        fontWeight: FontWeight.normal,
                        height: 1),
                  ),
                ),
              ),
            ),
          ),
        ],
      )),
      debugShowCheckedModeBanner: false,
    );
  }
}
