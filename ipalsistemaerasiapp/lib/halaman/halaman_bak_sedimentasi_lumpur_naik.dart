import 'package:flutter/material.dart';

class BakSedimentasiLumpurNaik extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          body: ListView(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(top: 35, left: 33),
            child: Align(
              alignment: Alignment.topRight,
              child: SizedBox(
                height: 33,
                child: TextButton(
                  child: const Text(
                    'Kembali Ke Halaman Sebelumnya',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Color(0xFF000000),
                        fontFamily: 'Roboto',
                        fontSize: 11,
                        letterSpacing: 0,
                        fontWeight: FontWeight.normal,
                        height: 1),
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 35, left: 33),
            child: Align(
              alignment: Alignment.topLeft,
              child: SizedBox(
                child: Text(
                  "Lumpur di Bak Sedimentasi Naik",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      color: Color(0xFF000000),
                      fontFamily: 'Roboto',
                      fontSize: 24,
                      letterSpacing: 0,
                      fontWeight: FontWeight.normal,
                      height: 1),
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(right: 33, top: 40, left: 33),
            child: Align(
              alignment: Alignment.topLeft,
              child: SizedBox(
                child: Text(
                  "Penyebab:",
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                      color: Color.fromRGBO(0, 0, 0, 1),
                      fontFamily: 'Roboto',
                      fontSize: 18,
                      letterSpacing: 0,
                      fontWeight: FontWeight.normal,
                      height: 1),
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(right: 33, top: 10, left: 33),
            child: Align(
              alignment: Alignment.topLeft,
              child: SizedBox(
                child: Text(
                  "Proses Pengolahan Air Limbah Belum Selesai Sempurna (Terutama Proses Denitrifikasi); (Lumpur Berwarna Cokelat dan Tidak Licin)",
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                      color: Color.fromRGBO(0, 0, 0, 1),
                      fontFamily: 'Roboto',
                      fontSize: 18,
                      letterSpacing: 0,
                      fontWeight: FontWeight.normal,
                      height: 1),
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(right: 33, top: 10, left: 33),
            child: Align(
              alignment: Alignment.topLeft,
              child: SizedBox(
                child: Text(
                  "Lumpur Septik (Lumpur Bau); Lumpur Aktif Mati (Lumpur Berwarna Abu/Hitam); Terdapat Lemak pada Bak Aerasi (Lumpur Glossy atau Licin)",
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                      color: Color.fromRGBO(0, 0, 0, 1),
                      fontFamily: 'Roboto',
                      fontSize: 18,
                      letterSpacing: 0,
                      fontWeight: FontWeight.normal,
                      height: 1),
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(right: 33, top: 20, left: 33),
            child: Align(
              alignment: Alignment.topLeft,
              child: SizedBox(
                child: Text(
                  "Solusi: Proses Pengolahan Air Limbah Belum Selesai Sempurna",
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                      color: Color.fromRGBO(0, 0, 0, 1),
                      fontFamily: 'Roboto',
                      fontSize: 18,
                      letterSpacing: 0,
                      fontWeight: FontWeight.normal,
                      height: 1),
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(right: 33, top: 10, left: 33),
            child: Align(
              alignment: Alignment.topLeft,
              child: SizedBox(
                child: Text(
                  "1. Lumpur diambil dan dimasukkan kembali ke tengah pipa inlet bak sedimentasi",
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                      color: Color.fromRGBO(0, 0, 0, 1),
                      fontFamily: 'Roboto',
                      fontSize: 18,
                      letterSpacing: 0,
                      fontWeight: FontWeight.normal,
                      height: 1),
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(right: 33, top: 10, left: 33),
            child: Align(
              alignment: Alignment.topLeft,
              child: SizedBox(
                child: Text(
                  "2. Aerator DiNyalakan Selama 24 Jam Hingga Busa di Bak Aerasi Menghilang (Jika Terdapat Busa Cokelat Gelap di Bak Aerasi)",
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                      color: Color.fromRGBO(0, 0, 0, 1),
                      fontFamily: 'Roboto',
                      fontSize: 18,
                      letterSpacing: 0,
                      fontWeight: FontWeight.normal,
                      height: 1),
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(right: 33, top: 10, left: 33),
            child: Align(
              alignment: Alignment.topLeft,
              child: SizedBox(
                child: Text(
                  "3. Overflow Bak Sedimentasi diNyalakan",
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                      color: Color.fromRGBO(0, 0, 0, 1),
                      fontFamily: 'Roboto',
                      fontSize: 18,
                      letterSpacing: 0,
                      fontWeight: FontWeight.normal,
                      height: 1),
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(right: 33, top: 10, left: 33),
            child: Align(
              alignment: Alignment.topLeft,
              child: SizedBox(
                child: Text(
                  "4. Aerator DiJadwalkan Dimatikan Maksimal 2 Jam Hingga Lumpur di Bak Sedimentasi Tidak Ada Lagi",
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                      color: Color.fromRGBO(0, 0, 0, 1),
                      fontFamily: 'Roboto',
                      fontSize: 18,
                      letterSpacing: 0,
                      fontWeight: FontWeight.normal,
                      height: 1),
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(right: 33, top: 20, left: 33),
            child: Align(
              alignment: Alignment.topLeft,
              child: SizedBox(
                child: Text(
                  "Solusi: Lumpur Septik",
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                      color: Color.fromRGBO(0, 0, 0, 1),
                      fontFamily: 'Roboto',
                      fontSize: 18,
                      letterSpacing: 0,
                      fontWeight: FontWeight.normal,
                      height: 1),
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(right: 33, top: 10, left: 33),
            child: Align(
              alignment: Alignment.topLeft,
              child: SizedBox(
                child: Text(
                  "1. Lumpur diambil dan dibuang",
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                      color: Color.fromRGBO(0, 0, 0, 1),
                      fontFamily: 'Roboto',
                      fontSize: 18,
                      letterSpacing: 0,
                      fontWeight: FontWeight.normal,
                      height: 1),
                ),
              ),
            ),
          ),
        ],
      )),
      debugShowCheckedModeBanner: false,
    );
  }
}
