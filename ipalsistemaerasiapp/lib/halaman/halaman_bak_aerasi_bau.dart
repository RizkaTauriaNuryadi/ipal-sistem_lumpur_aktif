import 'package:flutter/material.dart';

class BakAerasiBau extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          body: ListView(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(top: 35, left: 33),
            child: Align(
              alignment: Alignment.topRight,
              child: SizedBox(
                height: 33,
                child: TextButton(
                  child: const Text(
                    'Kembali Ke Halaman Sebelumnya',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Color(0xFF000000),
                        fontFamily: 'Roboto',
                        fontSize: 11,
                        letterSpacing: 0,
                        fontWeight: FontWeight.normal,
                        height: 1),
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 35, left: 33),
            child: Align(
              alignment: Alignment.topLeft,
              child: SizedBox(
                child: Text(
                  "Bak Aerasi Bau",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      color: Color(0xFF000000),
                      fontFamily: 'Roboto',
                      fontSize: 24,
                      letterSpacing: 0,
                      fontWeight: FontWeight.normal,
                      height: 1),
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(right: 33, top: 40, left: 33),
            child: Align(
              alignment: Alignment.topLeft,
              child: SizedBox(
                child: Text(
                  "Penyebab:",
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                      color: Color.fromRGBO(0, 0, 0, 1),
                      fontFamily: 'Roboto',
                      fontSize: 18,
                      letterSpacing: 0,
                      fontWeight: FontWeight.normal,
                      height: 1),
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(right: 33, top: 10, left: 33),
            child: Align(
              alignment: Alignment.topLeft,
              child: SizedBox(
                child: Text(
                  "Air limbah yang akan diolah terlalu banyak dan oksigen untuk mengolah air limbah tahap kedua tidak cukup",
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                      color: Color.fromRGBO(0, 0, 0, 1),
                      fontFamily: 'Roboto',
                      fontSize: 18,
                      letterSpacing: 0,
                      fontWeight: FontWeight.normal,
                      height: 1),
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(right: 33, top: 20, left: 33),
            child: Align(
              alignment: Alignment.topLeft,
              child: SizedBox(
                child: Text(
                  "Solusi:",
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                      color: Color.fromRGBO(0, 0, 0, 1),
                      fontFamily: 'Roboto',
                      fontSize: 18,
                      letterSpacing: 0,
                      fontWeight: FontWeight.normal,
                      height: 1),
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(right: 33, top: 10, left: 33),
            child: Align(
              alignment: Alignment.topLeft,
              child: SizedBox(
                child: Text(
                  "1. Pengaliran Air Limbah dari Bak Trap ke Bak Aerasi diHentikan",
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                      color: Color.fromRGBO(0, 0, 0, 1),
                      fontFamily: 'Roboto',
                      fontSize: 18,
                      letterSpacing: 0,
                      fontWeight: FontWeight.normal,
                      height: 1),
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(right: 33, top: 10, left: 33),
            child: Align(
              alignment: Alignment.topLeft,
              child: SizedBox(
                child: Text(
                  "2. Aerator DiNyalakan Selama 24 Jam Hingga Bau di Bak Aerasi Menghilang",
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                      color: Color.fromRGBO(0, 0, 0, 1),
                      fontFamily: 'Roboto',
                      fontSize: 18,
                      letterSpacing: 0,
                      fontWeight: FontWeight.normal,
                      height: 1),
                ),
              ),
            ),
          ),
        ],
      )),
      debugShowCheckedModeBanner: false,
    );
  }
}
