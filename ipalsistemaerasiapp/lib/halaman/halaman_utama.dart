import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ipalsistemaerasiapp/halaman/halaman_letak_permasalahan.dart';
import 'package:ipalsistemaerasiapp/halaman/halaman_prosedur_awal_limbah_masuk.dart';

class HalamanUtama extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.brown,
        body: Center(
          child: ListView(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(top: 100),
                child: Align(
                  alignment: Alignment.topCenter,
                  child: SizedBox(
                    height: 100,
                  ),
                ),
              ),
              Container(
                padding:
                    EdgeInsets.only(right: 61, top: 394 - 216 - 86, left: 61),
                child: Align(
                  alignment: Alignment.center,
                  child: SizedBox(
                    width: 239,
                    height: 62,
                    child: MaterialButton(
                      minWidth: 239,
                      height: 62,
                      color: Color(0xFFFFFFFF),
                      onPressed: () {
                        Get.to(HalamanProsedurAwalLimbahMasuk());
                      },
                      child: Text(
                        'Prosedur Awal Limbah Masuk',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Colors.brown,
                            fontFamily: 'Roboto',
                            fontSize: 18,
                            letterSpacing: 0,
                            fontWeight: FontWeight.normal,
                            height: 1),
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                padding:
                    EdgeInsets.only(right: 61, top: 474 - 394 - 31, left: 61),
                child: Align(
                  alignment: Alignment.center,
                  child: SizedBox(
                    width: 239,
                    height: 62,
                    child: MaterialButton(
                      minWidth: 239,
                      height: 62,
                      color: Colors.brown,
                      onPressed: () {
                        Get.to(LetakPermasalahan());
                      },
                      child: Text(
                        'Troubleshooting Permasalahan IPAL',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Color(0xFFFFFFFF),
                            fontFamily: 'Roboto',
                            fontSize: 18,
                            letterSpacing: 0,
                            fontWeight: FontWeight.normal,
                            height: 1),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      debugShowCheckedModeBanner: false,
    );
  }
}
