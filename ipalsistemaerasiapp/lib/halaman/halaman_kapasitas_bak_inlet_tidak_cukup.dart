import 'package:flutter/material.dart';

class KapasitasBakInletTidakCukup extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          body: ListView(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(top: 35, left: 33),
            child: Align(
              alignment: Alignment.topRight,
              child: SizedBox(
                height: 33,
                child: TextButton(
                  child: const Text(
                    'Kembali Ke Halaman Sebelumnya',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Color(0xFF000000),
                        fontFamily: 'Roboto',
                        fontSize: 11,
                        letterSpacing: 0,
                        fontWeight: FontWeight.normal,
                        height: 1),
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 35, left: 33),
            child: Align(
              alignment: Alignment.topLeft,
              child: SizedBox(
                child: Text(
                  "Kapasitas Bak Inlet Tidak Cukup",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      color: Color(0xFF000000),
                      fontFamily: 'Roboto',
                      fontSize: 24,
                      letterSpacing: 0,
                      fontWeight: FontWeight.normal,
                      height: 1),
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(right: 33, top: 40, left: 33),
            child: Align(
              alignment: Alignment.topLeft,
              child: SizedBox(
                child: Text(
                  "Penyebab:",
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                      color: Color.fromRGBO(0, 0, 0, 1),
                      fontFamily: 'Roboto',
                      fontSize: 18,
                      letterSpacing: 0,
                      fontWeight: FontWeight.normal,
                      height: 1),
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(right: 33, top: 10, left: 33),
            child: Align(
              alignment: Alignment.topLeft,
              child: SizedBox(
                child: Text(
                  "Bak inlet terlalu penuh dan diestimasikan tidak cukup untuk menerima air limbah yang akan datang",
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                      color: Color.fromRGBO(0, 0, 0, 1),
                      fontFamily: 'Roboto',
                      fontSize: 18,
                      letterSpacing: 0,
                      fontWeight: FontWeight.normal,
                      height: 1),
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(right: 33, top: 20, left: 33),
            child: Align(
              alignment: Alignment.topLeft,
              child: SizedBox(
                child: Text(
                  "Solusi:",
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                      color: Color.fromRGBO(0, 0, 0, 1),
                      fontFamily: 'Roboto',
                      fontSize: 18,
                      letterSpacing: 0,
                      fontWeight: FontWeight.normal,
                      height: 1),
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(right: 33, top: 10, left: 33),
            child: Align(
              alignment: Alignment.topLeft,
              child: SizedBox(
                child: Text(
                  "1. ByPass Air Limbah ke Sungai Jika Air Limbah Tidak Berbau dan Berwarna Susu (Opsional)",
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                      color: Color.fromRGBO(0, 0, 0, 1),
                      fontFamily: 'Roboto',
                      fontSize: 18,
                      letterSpacing: 0,
                      fontWeight: FontWeight.normal,
                      height: 1),
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(right: 33, top: 10, left: 33),
            child: Align(
              alignment: Alignment.topLeft,
              child: SizedBox(
                child: Text(
                  "2. Buang Air Limbah ke Kebun Jika Memungkinkan (Opsional)",
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                      color: Color.fromRGBO(0, 0, 0, 1),
                      fontFamily: 'Roboto',
                      fontSize: 18,
                      letterSpacing: 0,
                      fontWeight: FontWeight.normal,
                      height: 1),
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(right: 33, top: 10, left: 33),
            child: Align(
              alignment: Alignment.topLeft,
              child: SizedBox(
                child: Text(
                  "3. Naikan Debit Transfer ke Bak Aerasi menjadi 1 m3/jam dari 0,5 m3/jam sampai Air Limbah CIP yang akan datang habis teraliri ke Bak Inlet",
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                      color: Color.fromRGBO(0, 0, 0, 1),
                      fontFamily: 'Roboto',
                      fontSize: 18,
                      letterSpacing: 0,
                      fontWeight: FontWeight.normal,
                      height: 1),
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(right: 33, top: 10, left: 33),
            child: Align(
              alignment: Alignment.topLeft,
              child: SizedBox(
                child: Text(
                  "4. Aerator DiNyalakan Selama 24 Jam Hingga Debit DiTurunkan Kembali",
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                      color: Color.fromRGBO(0, 0, 0, 1),
                      fontFamily: 'Roboto',
                      fontSize: 18,
                      letterSpacing: 0,
                      fontWeight: FontWeight.normal,
                      height: 1),
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(right: 33, top: 10, left: 33),
            child: Align(
              alignment: Alignment.topLeft,
              child: SizedBox(
                child: Text(
                  "5. Turunkan Debit Transfer ke Bak Aerasi kembali menjadi 0,5 m3/jam setelah Air Limbah CIP yang akan datang habis teraliri dan Bak Inlet dirasa cukup untuk menampung kedepannya",
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                      color: Color.fromRGBO(0, 0, 0, 1),
                      fontFamily: 'Roboto',
                      fontSize: 18,
                      letterSpacing: 0,
                      fontWeight: FontWeight.normal,
                      height: 1),
                ),
              ),
            ),
          ),
        ],
      )),
      debugShowCheckedModeBanner: false,
    );
  }
}
