import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:ipalsistemaerasiapp/halaman/halaman_bak_aerasi.dart';
import 'package:ipalsistemaerasiapp/halaman/halaman_bak_inlet.dart';
import 'package:ipalsistemaerasiapp/halaman/halaman_bak_sedimentasi.dart';
import 'package:ipalsistemaerasiapp/halaman/halaman_bak_trap.dart';
import 'package:ipalsistemaerasiapp/halaman/halaman_bak_treated_water.dart';

class LetakPermasalahan extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          body: ListView(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(top: 35, left: 33),
            child: Align(
              alignment: Alignment.topRight,
              child: SizedBox(
                height: 33,
                child: TextButton(
                  child: const Text(
                    'Kembali Ke Halaman Sebelumnya',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Color(0xFF000000),
                        fontFamily: 'Roboto',
                        fontSize: 11,
                        letterSpacing: 0,
                        fontWeight: FontWeight.normal,
                        height: 1),
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 35),
            child: Align(
              alignment: Alignment.topCenter,
              child: SizedBox(
                height: 33,
                child: Text(
                  "Letak Permasalahan",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Color(0xFF000000),
                      fontFamily: 'Roboto',
                      fontSize: 24,
                      letterSpacing: 0,
                      fontWeight: FontWeight.normal,
                      height: 1),
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(right: 33, top: 40, left: 33),
            child: Align(
              alignment: Alignment.topCenter,
              child: SizedBox(
                child: MaterialButton(
                  minWidth: 239,
                  height: 62,
                  color: Color(0xFFFFFFFF),
                  onPressed: () {
                    Get.to(BakInlet());
                  },
                  child: Text(
                    "Bak Inlet",
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                        color: Color.fromRGBO(0, 0, 0, 1),
                        fontFamily: 'Roboto',
                        fontSize: 18,
                        letterSpacing: 0,
                        fontWeight: FontWeight.normal,
                        height: 1),
                  ),
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(right: 33, top: 40, left: 33),
            child: Align(
              alignment: Alignment.topCenter,
              child: SizedBox(
                child: MaterialButton(
                  minWidth: 239,
                  height: 62,
                  color: Color(0xFFFFFFFF),
                  onPressed: () {
                    Get.to(BakTrap());
                  },
                  child: Text(
                    "Bak Trap",
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                        color: Color.fromRGBO(0, 0, 0, 1),
                        fontFamily: 'Roboto',
                        fontSize: 18,
                        letterSpacing: 0,
                        fontWeight: FontWeight.normal,
                        height: 1),
                  ),
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(right: 33, top: 40, left: 33),
            child: Align(
              alignment: Alignment.topCenter,
              child: SizedBox(
                child: MaterialButton(
                  minWidth: 239,
                  height: 62,
                  color: Color(0xFFFFFFFF),
                  onPressed: () {
                    Get.to(BakAerasi());
                  },
                  child: Text(
                    "Bak Aerasi",
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                        color: Color.fromRGBO(0, 0, 0, 1),
                        fontFamily: 'Roboto',
                        fontSize: 18,
                        letterSpacing: 0,
                        fontWeight: FontWeight.normal,
                        height: 1),
                  ),
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(right: 33, top: 40, left: 33),
            child: Align(
              alignment: Alignment.topCenter,
              child: SizedBox(
                child: MaterialButton(
                  minWidth: 239,
                  height: 62,
                  color: Color(0xFFFFFFFF),
                  onPressed: () {
                    Get.to(BakSedimentasi());
                  },
                  child: Text(
                    "Bak Sedimentasi",
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                        color: Color.fromRGBO(0, 0, 0, 1),
                        fontFamily: 'Roboto',
                        fontSize: 18,
                        letterSpacing: 0,
                        fontWeight: FontWeight.normal,
                        height: 1),
                  ),
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(right: 33, top: 40, left: 33),
            child: Align(
              alignment: Alignment.topCenter,
              child: SizedBox(
                child: MaterialButton(
                  minWidth: 239,
                  height: 62,
                  color: Color(0xFFFFFFFF),
                  onPressed: () {
                    Get.to(BakTreatedWater());
                  },
                  child: Text(
                    "Bak Treated Water",
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                        color: Color.fromRGBO(0, 0, 0, 1),
                        fontFamily: 'Roboto',
                        fontSize: 18,
                        letterSpacing: 0,
                        fontWeight: FontWeight.normal,
                        height: 1),
                  ),
                ),
              ),
            ),
          ),
        ],
      )),
      debugShowCheckedModeBanner: false,
    );
  }
}
